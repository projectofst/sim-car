import pyvjoy
import sys
import serial
from datetime import datetime

WATCHDOG_LIM = 200000 # microsecond

j = pyvjoy.VJoyDevice(1)

COM = sys.argv[1]
ser = serial.Serial(COM)

a = datetime.now()

while True:

#    try: 
        s = ser.readline()

        s = s.decode('ascii').split(',')

        if len(s) != 7:
            continue
        
        try:
            sid, dlc, *data, timpestamp = [int(x) for x in s]
        except Exception as e:
            continue    

        dev_id = sid & 0x1F
        msg_id = (sid >> 5)

        if dev_id == 10 and msg_id == 34:
            #print("data[0]", data[0])
            if data[0] > 2**15:
                data[0] = - ((data[0] ^ 0xFFFF) + 1)
            #print("data[0]", data[0]/10)    
            SE = data[0] / 3600
            #print("SE", SE)
            j.data.wAxisZ = int(0x4000 + SE * 0x8000) # SE 180 => max_value (0x8000); 0 => 0x4000
            #print(f"SE", j.data.wAxisZ/0x8000)
            a = datetime.now()
        elif dev_id == 9 and msg_id == 10:
            j.data.wAxisX = int(data[1] * 0x8000 / 10000) # APPS
            j.data.wAxisY = int(data[3] * 0x8000 / 10000) # BPSe
            #print(f"X:",j.data.wAxisX,"Y:",j.data.wAxisY)
            a = datetime.now()
        elif dev_id == 12 and msg_id >= 50:
            j.data.lButtons += 2**(msg_id - 50)
            #print(f"Buttons:",j.data.lButtons)
            a = datetime.now()
        else:
            delta = datetime.now() - a
            if delta.microseconds > WATCHDOG_LIM:
                j.reset_data()

        j.update()

#    except:
#        ser.close()
#        print("Error:")
#        sys.exit()

