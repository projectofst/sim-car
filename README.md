# Sim Car

In car Simulator. You can play any gameyou want that uses a joystick with our cars. Developed in the 10th generation of vehicles, changes must be made if the CAN messages changes.

# Installation

1. Install [Vjoy](http://vjoystick.sourceforge.net/site/index.php/download-a-install/download)
2. Install [Python 3](https://www.python.org/downloads/) 
3. Install pyvjoy through cmd command: `pip install pyvjoy`

# Run

1. Connect the CAN sniffer to PC
2. Run command: `python simcar.py <COM_PORT>`
3. Open "Monitor Vjoy" to check if you are getting the correct inputs on the virtual joystick

# To-do

- Compatibility with FCP
